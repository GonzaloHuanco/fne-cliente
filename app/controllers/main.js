'use strict';
/* App Module */
var fneApp = angular.module('fneApp', [
  	'ngRoute',
  	'ngResource',
  	'ui.router',
	'fneApp.CandidatasAddController',
	'fneApp.candidatasService'
])


fneApp.config(function($stateProvider, $httpProvider, $provide,$sceDelegateProvider) {
  	
  	$stateProvider.state('candidata', {
                    url: '/candidatas/add',
                    templateUrl: 'app/views/candidatas/add.html',
                    controller: 'CandidatasAddController'
                })
    .state('home', {
                    url: '/home',
                    templateUrl: 'app/views/home/home.html',
                    controller: 'MainCtrl'
                })
});  

fneApp.controller('MainCtrl', [ function (	) {
   

}]);

