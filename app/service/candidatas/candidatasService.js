'use strict';

angular.module('fneApp.candidatasService',['ngResource'])

.factory('Candidatas', function($resource) {
  return $resource('/api/entries/:id'); // Note the full endpoint address
  
});